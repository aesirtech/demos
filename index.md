---
layout: default
title: Datafox Demos
active: intro
---

## Demos showcasing the **Realtime Data Discovery**

The [**ACC claims**](acc) demo shows Claims data for the last several years

The [**MOE enrolments**](moe) demo shows the enrolments around the country

The [**MOTE import / exports**](mote) demo shows the trading NZ does with others
