---
# Front-matter so Jekyll processes it
---

var direction,
    country,
    commodity,
    period,
    value,
    direction_chart,
    country_chart,
    commodity_chart,
    period_chart,
    value_chart;

var dictionary = {};

var dateFormat = d3.time.format('%Y-%m');

showCharts();

function showCharts() {
  function labelAccessor(withValue) {
    return function(d) {
      label = d.key
      if (withValue) {
        label = label + ": " + Math.floor(d.value.value);
      }
      return label;
    }
  }

  direction = datafox.fakeCrossFilter().dim('direction').aggregation({target:'Value',type:'sum','name':'value'}).order({name:'value'});
  direction_chart = dc.pieChart('#direction')
    .innerRadius(60)
    .radius(100)
    .height(300)
    .dimension(direction)
    .group(direction)
    .transitionDuration(300)
    .valueAccessor(function(d){return d.value.value})
    .label(labelAccessor(false))
    .title(labelAccessor(true))

  country = datafox.fakeCrossFilter().dim('Country').aggregation({target:'Value',type:'sum','name':'value'}).order({name:'value'}).limit(24);
  country_chart = dc.rowChart('#country')
    .height(600)
    .dimension(country)
    .group(country)
    .transitionDuration(300)
    .valueAccessor(function(d){return d.value.value})
    .label(labelAccessor(false))
    .title(labelAccessor(true))
    .ordering(function(d){return -d.value.value})
    .elasticX(true);
  country_chart.xAxis().ticks(4).tickFormat(d3.format('s'));

  commodity = datafox.fakeCrossFilter().dim('Commodity').aggregation({target:'Value',type:'sum','name':'value'}).order({name:'value'}).limit(24);
  commodity_chart = dc.rowChart('#commodity')
    .height(600)
    .dimension(commodity)
    .group(commodity)
    .transitionDuration(300)
    .valueAccessor(function(d){return d.value.value})
    .label(labelAccessor(false))
    .title(labelAccessor(true))
    .ordering(function(d){return -d.value.value})
    .elasticX(true);
  commodity_chart.xAxis().ticks(4).tickFormat(d3.format('s'));

  year = datafox.fakeCrossFilter().dim('Period').aggregation({target:'Value',type:'sum','name':'value'}).order({name:'value'});
  year_chart = dc.barChart('#year')
    .margins({top: 10, right: 10, bottom: 20, left: 40})
    .height(300)
    .dimension(year)
    .group(year)
    .transitionDuration(300)
    .valueAccessor(function(d){return d.value.value})
    .x(d3.scale.linear().domain([1999,2015]))
    .centerBar(true)
    .xUnits(dc.units.integers)
    .ordering(function(d){ return d.key})
    .elasticY(true)

  year_chart.xAxis().ticks(12).tickFormat(function (d) {return d});
  year_chart.yAxis().tickFormat(d3.format('s')).ticks(12)

  // now the magic happens.
  datafox.registry.connect("{{ site.datafox_host }}",'mote', 'data');
  datafox.registry.registerAll();

  datafox.registry.fetchData(
    function() {
      dc.renderAll();
    }
  );
}

