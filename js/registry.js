//---
//# Front-matter so Jekyll processes it
//---

datafox.registry = function registry() {

  var config = {};

  function connect(url,index,collection) {
    config.url = url;
    config.index = index;
    config.collection = collection;
  }

  function somethingFiltered() {
    queueWork(function(){dc.redrawAll()});
  }

  // this chunk is in essence a smart debouncer
  var busy = false;
  var callbackInWaiting = undefined;

  function queueWork(callback) {
    callbackInWaiting = callback;
    queryIfNotBusy();
  }

  function queryIfNotBusy() {
    if (!busy && callbackInWaiting) {
      busy = true; // if we were not before, we are now...
      callback = callbackInWaiting;
      callbackInWaiting = undefined;
      fetchData(callback);
    }
  }
  // end smart debouncer

  function forAllFakeCharts(f) {
    var result = [];
    var list = dc.chartRegistry.list();
    for (var i in list) {
      chart = list[i]
      if (chart.group().isFake) {
        result.push(f(chart));
      }
    }
    return result;
  }

  function getData() {
    forAllFakeCharts(function (c) {
      makeMatch(chart);
    })
  }

  function register(chart) {
    // replace filter handler in dc.js so it doesn't make 2 calls to filter each time a filter happens.
    chart.filterHandler(
        function (dimension, filters) {
          // dimension.filter(null); // bad filter handler! no biscuit!
          if (filters.length === 0) {
            dimension.filter(null);
          } else {
            dimension.filterFunction(function (d) {
              for (var i = 0; i < filters.length; i++) {
                var filter = filters[i];
                if (filter.isFiltered && filter.isFiltered(d)) {
                  return true;
                } else if (filter <= d && filter >= d) {
                  return true;
                }
              }
              return false;
            });
          }
          return filters;
        }
    )
    chart.group().name(chart.anchorName());
    chart.group().on('filterChanged.fake', somethingFiltered);
  }

  function registerAll() {
    forAllFakeCharts(register)
  }

  function findGroup(chartName) {
    var list = dc.chartRegistry.list();
    for (var i in list) {
      chart = list[i]
      if (chart.anchorName() == chartName) {
        return chart.group();
      }
    }
  }

  function processResponse(res) {
    res = JSON.parse(res);
    for (chartid in res) {
      var chart = res[chartid];
      findGroup(chart.name).data(chart.data);
    }
  }

  function generateFilters(filters) {
    terms = [];
    range = {};
    for (i in filters) {
      filter = filters[i];
      if (Array.isArray(filter)) {
        range = {from:filter[0], to:filter[1]};
        //console.log(range)
        //ranges.push(range);
      } else {
        terms.push(filter);
      }
    }
    return {terms:terms,range:range};
  }

  function fetchData(callback) {
    document.body.className = 'wait';
    var request = forAllFakeCharts(
        function(chart){
          var config = JSON.parse(JSON.stringify(chart.group().configuration())); //clone
          delete config.data; // don't sent THAT to the server. opps....
          config.filters = generateFilters(chart.filters());
          return config;
        }
    );

    postQuery(request, function(result){
      processResponse(result);
      callback();
      busy = false; // finished the work
      document.body.className = '';
      queryIfNotBusy();
    })
  }

  function postQuery(json, callback, queryType) {
    var qt = queryType || 'agg'
    var data = JSON.stringify(json);
    var xhr = new XMLHttpRequest();
    xhr.open('POST', config.url + '/' + qt + '/' + config.index + "/" + config.collection);
    xhr.addEventListener('load', function (e) {
      res = e.target.response;
      callback(res);
    }, false);
    xhr.setRequestHeader('Content-Type', 'application/json');
    xhr.send(data);
  }

  function scan(fileType) {
    fileType = fileType || 'csv';
    var request = forAllFakeCharts(
        function(chart){
          var config = JSON.parse(JSON.stringify(chart.group().configuration())); //clone
          delete config.data; // don't send our current data to the server....
          config.filters = generateFilters(chart.filters());
          return config;
        }
    )
    postQuery(request, function(result){
      //result = JSON.parse(result);
      $.fileDownload(config.url + result);
//      $.fileDownload("{{ site.datafox_host }}" + '/files/'+ result.fileName);
    }, 'dataurl/' + fileType)
  }

  registry.connect = connect;
  registry.registerAll = registerAll;
  registry.register = register;
  registry.fetchData = fetchData;
  registry.scan = scan;
  return registry;

}();


// var encrypted = CryptoJS.AES.encrypt(message, "datacat");
// CryptoJS.AES.decrypt(encrypted, "datacat").toString(CryptoJS.enc.Utf8)
