datafox = {}

function fakeCrossFilter(config) {
  config = config || {};
  config.data = config.data || [];
  config.limit = config.limit || 0;
  config.type = config.type || 'terms'; // interval, minBounds, maxBounds should be part of type.
  config.aggregations = config.aggregations || [];

  var dispatch = d3.dispatch('filterChanged', 'groupAll', 'groupTop', 'dataChanged', 'processStagesChange');

  function configuration(_) {
    if (!arguments.length) { return config };
    config = _;
    return fake;
  }

  function name(_) {
    if (!arguments.length) { return config.name };
    config.name = _;
    return fake;
  }

  function order(_) {
    if (!arguments.length) { return config.order };
    config.order = _;
    return fake;
  }

  function aggregations(_) {
    if (!arguments.length) { return config.aggregations };
    config.aggregations = _;
    return fake;
  }

  function aggregation(_) {
    config.aggregations.push(_);
    return fake;
  }

  function dim(name) {
    if (!arguments.length) { return config.dim };
    config.dim = name;
    return fake;
  }

  function type(_) {
    if (!arguments.length) { return config.type };
    config.type = _;
    return fake;
  }

  function limit(_) {
    if (!arguments.length) { return config.limit };
    config.limit = _;
    return fake;
  }

  function data(_) {
    if (!arguments.length) { return config.data };
    config.data = _;
    dispatch.dataChanged(_);
    return fake;
  }

  // crossfilter emulation related stuff
  function all() {
    dispatch.groupAll(config.data);
    return config.data;
  }

  function top(e) {
    dispatch.groupTop(config.data);
    return config.data.slice(0, e);
  }

  // generate fake
  var fake = {};

  // metadata
  fake.isFake = true;
  d3.rebind(fake, dispatch, 'on');
  fake.filterChanged = dispatch.filterChanged;

  // datafox
  fake.name = name;
  fake.dim = dim;
  fake.limit = limit;
  fake.type = type;
  fake.configuration = configuration;
  fake.aggregation = aggregation;
  fake.aggregations = aggregations;
  fake.order = order;

  // data
  fake.data = data;

  // group and dimension emulation
  fake.all = all;
  fake.top = top;
  fake.filter = dispatch.filterChanged;
  fake.filterExact = dispatch.filterChanged;
  fake.filterRange = dispatch.filterChanged;
  fake.filterFunction = dispatch.filterChanged;
  fake.filterAll = dispatch.filterChanged;

  return fake;
}

datafox.fakeCrossFilter = fakeCrossFilter;
