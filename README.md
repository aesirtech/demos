# Jekyll powered Demo site [aesir-demos](www.aesir.co.nz/demos)

## Setup

* install rvm if required
* rvm install 2.2.1
* gem install bundler
* bundle install

## Running

* jekyll server --config _config_dev.yml --watch


## Deployment

* git push origin - github handles the rest
