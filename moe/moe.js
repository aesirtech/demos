---
# Front-matter so Jekyll processes it
---

var students,
    year,
    subject_learning_area,
    subject_description,
    student_gender,
    student_year_level,
    school_name,
    school_sector,
    school_type,
    school_authority,
    school_decile,
    school_gender,
    school_affiliation_type,
    school_affiliation,
    school_definition,
    school_kura_type,
    region_education_region,
    region_regional_council,
    region_territorial_authority,
    region_MOE_local_office;

var students_chart,
    year_chart,
    subject_learning_area_chart,
    subject_description_chart,
    student_gender_chart,
    student_year_level_chart,
    school_name_chart,
    school_sector_chart,
    school_type_chart,
    school_authority_chart,
    school_decile_chart,
    school_gender_chart,
    school_affiliation_type_chart,
    school_affiliation_chart,
    school_definition_chart,
    school_kura_type_chart,
    region_education_region_chart,
    region_regional_council_chart,
    region_territorial_authority_chart,
    region_MOE_local_office_chart;


var dateFormat = d3.time.format('%Y-%m');

showCharts();

function showCharts() {
  function labelAccessor(withValue) {
    return function(d) {
      label = d.key
      if (withValue) {
        label = label + ": " + Math.floor(d.value.value);
      }
      return label;
    }
  }

var year = datafox.fakeCrossFilter().dim('year').aggregation({target:'students',type:'sum','name':'value'}),
    subject_learning_area = datafox.fakeCrossFilter().dim('subject_learning_area').aggregation({target:'students',type:'sum','name':'value'}).limit(12).order({name:'value'}),
    subject_description = datafox.fakeCrossFilter().dim('subject_description').aggregation({target:'students',type:'sum','name':'value'}).limit(12).order({name:'value'}),
    student_gender = datafox.fakeCrossFilter().dim('student_gender').aggregation({target:'students',type:'sum','name':'value'}).limit(12).order({name:'value'}),
    student_year_level = datafox.fakeCrossFilter().dim('student_year_level').aggregation({target:'students',type:'sum','name':'value'}).limit(12).order({name:'value'}),
    school_name = datafox.fakeCrossFilter().dim('school_name').aggregation({target:'students',type:'sum','name':'value'}).limit(12).order({name:'value'}),
    school_sector = datafox.fakeCrossFilter().dim('school_sector').aggregation({target:'students',type:'sum','name':'value'}).limit(12).order({name:'value'}),
    school_type = datafox.fakeCrossFilter().dim('school_type').aggregation({target:'students',type:'sum','name':'value'}).limit(12).order({name:'value'}),
    school_authority = datafox.fakeCrossFilter().dim('school_authority').aggregation({target:'students',type:'sum','name':'value'}).limit(12).order({name:'value'}),
    school_decile = datafox.fakeCrossFilter().dim('school_decile').aggregation({target:'students',type:'sum','name':'value'}).limit(12).order({name:'value'}),
    school_gender = datafox.fakeCrossFilter().dim('school_gender').aggregation({target:'students',type:'sum','name':'value'}).limit(12).order({name:'value'}),
    school_affiliation_type = datafox.fakeCrossFilter().dim('school_affiliation_type').aggregation({target:'students',type:'sum','name':'value'}).limit(12).order({name:'value'}),
    school_affiliation = datafox.fakeCrossFilter().dim('school_affiliation').aggregation({target:'students',type:'sum','name':'value'}).limit(12).order({name:'value'}),
    school_definition = datafox.fakeCrossFilter().dim('school_definition').aggregation({target:'students',type:'sum','name':'value'}).limit(12).order({name:'value'}),
    school_kura_type = datafox.fakeCrossFilter().dim('school_kura_type').aggregation({target:'students',type:'sum','name':'value'}).limit(12).order({name:'value'}),
    region_education_region = datafox.fakeCrossFilter().dim('region_education_region').aggregation({target:'students',type:'sum','name':'value'}).limit(12).order({name:'value'}),
    region_regional_council = datafox.fakeCrossFilter().dim('region_regional_council').aggregation({target:'students',type:'sum','name':'value'}).limit(12).order({name:'value'}),
    region_territorial_authority = datafox.fakeCrossFilter().dim('region_territorial_authority').aggregation({target:'students',type:'sum','name':'value'}).limit(12).order({name:'value'}),
    region_MOE_local_office = datafox.fakeCrossFilter().dim('region_MOE_local_office').aggregation({target:'students',type:'sum','name':'value'}).limit(12).order({name:'value'});

  function darker_colors() {
    var colors = d3.scale.category20c()
    return function(d) {
      color = d3.rgb(colors(d));
      return color.darker(2).toString();
    }
  }


  subject_learning_area_chart = dc.rowChart('#subject_learning_area')
    .height(300)
    .colors(darker_colors())
    .dimension(subject_learning_area)
    .group(subject_learning_area)
    .transitionDuration(300)
    .valueAccessor(function(d){return d.value.value})
    .label(labelAccessor(false))
    .title(labelAccessor(true))
    .ordering(function(d){return -d.value.value})
    .elasticX(true);

  subject_description_chart = dc.rowChart('#subject_description')
    .height(300)
    .colors(darker_colors())
    .dimension(subject_description)
    .group(subject_description)
    .transitionDuration(300)
    .valueAccessor(function(d){return d.value.value})
    .label(labelAccessor(false))
    .title(labelAccessor(true))
    .ordering(function(d){return -d.value.value})
    .elasticX(true);

  student_gender_chart = dc.pieChart('#student_gender')
    .innerRadius(60)
    .radius(100)
    .height(300)
    .colors(darker_colors())
    .dimension(student_gender)
    .group(student_gender)
    .transitionDuration(300)
    .valueAccessor(function(d){return d.value.value})
    .label(labelAccessor(false))
    .title(labelAccessor(true))
    .ordering(function(d){return d.key});

  student_year_level_chart = dc.rowChart('#student_year_level')
    .height(300)
    .colors(darker_colors())
    .dimension(student_year_level)
    .group(student_year_level)
    .transitionDuration(300)
    .valueAccessor(function(d){return d.value.value})
    .label(labelAccessor(false))
    .title(labelAccessor(true))
    .ordering(function(d){return parseInt(d.key.match(/\d+/))})
    .elasticX(true);

  school_name_chart = dc.rowChart('#school_name')
    .height(300)
    .colors(darker_colors())
    .dimension(school_name)
    .group(school_name)
    .transitionDuration(300)
    .valueAccessor(function(d){return d.value.value})
    .label(labelAccessor(false))
    .title(labelAccessor(true))
    .ordering(function(d){return -d.value.value})
    .elasticX(true);

  school_sector_chart = dc.pieChart('#school_sector')
    .innerRadius(60)
    .radius(100)
    .height(300)
    .colors(darker_colors())
    .dimension(school_sector)
    .group(school_sector)
    .transitionDuration(300)
    .valueAccessor(function(d){return d.value.value})
    .label(labelAccessor(false))
    .title(labelAccessor(true));
  //  .ordering(function(d){return -d.value.value})

  school_type_chart = dc.rowChart('#school_type')
    .height(300)
    .colors(darker_colors())
    .dimension(school_type)
    .group(school_type)
    .transitionDuration(300)
    .valueAccessor(function(d){return d.value.value})
    .label(labelAccessor(false))
    .title(labelAccessor(true))
    .ordering(function(d){return -d.value.value})
    .elasticX(true);

  school_authority_chart = dc.rowChart('#school_authority')
    .height(300)
    .colors(darker_colors())
    .dimension(school_authority)
    .group(school_authority)
    .transitionDuration(300)
    .valueAccessor(function(d){return d.value.value})
    .label(labelAccessor(false))
    .title(labelAccessor(true))
  //  .ordering(function(d){return -d.value.value})
    .elasticX(true);

  school_decile_chart = dc.rowChart('#school_decile')
    .height(300)
    .colors(darker_colors())
    .dimension(school_decile)
    .group(school_decile)
    .transitionDuration(300)
    .valueAccessor(function(d){return d.value.value})
    .label(labelAccessor(false))
    .title(labelAccessor(true))
    .ordering(function(d){
      i = parseInt(d.key.match(/\d+/));
      if (isNaN(i)) {
        i = 999;
      }
      return i;
    })
    .elasticX(true);

  school_gender_chart = dc.pieChart('#school_gender')
    .innerRadius(60)
    .radius(100)
    .height(300)
    .colors(darker_colors())
    .dimension(school_gender)
    .transitionDuration(300)
    .group(school_gender)
    .valueAccessor(function(d){return d.value.value})
    .label(labelAccessor(false))
    .title(labelAccessor(true))
    .ordering(function(d){return d.key});

  school_affiliation_type_chart = dc.pieChart('#school_affiliation_type')
    .innerRadius(60)
    .radius(100)
    .height(300)
    .colors(darker_colors())
    .dimension(school_affiliation_type)
    .group(school_affiliation_type)
    .transitionDuration(300)
    .valueAccessor(function(d){return d.value.value})
    .label(labelAccessor(false))
    .title(labelAccessor(true))
    .ordering(function(d){return -d.value.value});

  school_affiliation_chart = dc.rowChart('#school_affiliation')
    .height(300)
    .colors(darker_colors())
    .dimension(school_affiliation)
    .group(school_affiliation)
    .transitionDuration(300)
    .valueAccessor(function(d){return d.value.value})
    .label(labelAccessor(false))
    .title(labelAccessor(true))
    .ordering(function(d){return -d.value.value})
    .elasticX(true);

  school_definition_chart = dc.rowChart('#school_definition')
    .height(300)
    .colors(darker_colors())
    .dimension(school_definition)
    .group(school_definition)
    .transitionDuration(300)
    .valueAccessor(function(d){return d.value.value})
    .label(labelAccessor(false))
    .title(labelAccessor(true))
    .ordering(function(d){return -d.value.value})
    .elasticX(true);

  school_kura_type_chart = dc.pieChart('#school_kura_type')
    .innerRadius(60)
    .radius(100)
    .height(300)
    .colors(darker_colors())
    .dimension(school_kura_type)
    .group(school_kura_type)
    .transitionDuration(300)
    .valueAccessor(function(d){return d.value.value})
    .label(labelAccessor(false))
    .title(labelAccessor(true))
    .ordering(function(d){return -d.value.value});

  region_education_region_chart = dc.rowChart('#region_education_region')
    .height(300)
    .colors(darker_colors())
    .dimension(region_education_region)
    .group(region_education_region)
    .transitionDuration(300)
    .valueAccessor(function(d){return d.value.value})
    .label(labelAccessor(false))
    .title(labelAccessor(true))
    .ordering(function(d){return -d.value.value})
    .elasticX(true);

  region_regional_council_chart = dc.rowChart('#region_regional_council')
    .height(300)
    .colors(darker_colors())
    .dimension(region_regional_council)
    .group(region_regional_council)
    .transitionDuration(300)
    .valueAccessor(function(d){return d.value.value})
    .label(labelAccessor(false))
    .title(labelAccessor(true))
    .ordering(function(d){return -d.value.value})
    .elasticX(true);

  region_territorial_authority_chart = dc.rowChart('#region_territorial_authority')
    .height(300)
    .colors(darker_colors())
    .dimension(region_territorial_authority)
    .group(region_territorial_authority)
    .transitionDuration(300)
    .valueAccessor(function(d){return d.value.value})
    .label(labelAccessor(false))
    .title(labelAccessor(true))
    .ordering(function(d){return -d.value.value})
    .elasticX(true);

  region_MOE_local_office_chart = dc.rowChart('#region_MOE_local_office')
    .height(300)
    .colors(darker_colors())
    .dimension(region_MOE_local_office)
    .group(region_MOE_local_office)
    .transitionDuration(300)
    .valueAccessor(function(d){return d.value.value})
    .label(labelAccessor(false))
    .title(labelAccessor(true))
    .ordering(function(d){return -d.value.value})
    .elasticX(true);

  subject_learning_area_chart.xAxis().ticks(4).tickFormat(d3.format('s'));
  subject_description_chart.xAxis().ticks(4).tickFormat(d3.format('s'));
  student_year_level_chart.xAxis().ticks(4).tickFormat(d3.format('s'));
  school_name_chart.xAxis().ticks(4).tickFormat(d3.format('s'));
  school_type_chart.xAxis().ticks(4).tickFormat(d3.format('s'));
  school_authority_chart.xAxis().ticks(4).tickFormat(d3.format('s'));
  school_decile_chart.xAxis().ticks(4).tickFormat(d3.format('s'));
  school_affiliation_chart.xAxis().ticks(4).tickFormat(d3.format('s'));
  school_definition_chart.xAxis().ticks(4).tickFormat(d3.format('s'));
  region_education_region_chart.xAxis().ticks(4).tickFormat(d3.format('s'));
  region_regional_council_chart.xAxis().ticks(4).tickFormat(d3.format('s'));
  region_territorial_authority_chart.xAxis().ticks(4).tickFormat(d3.format('s'));
  region_MOE_local_office_chart.xAxis().ticks(4).tickFormat(d3.format('s'));

  year_chart = dc.barChart('#year')
    .margins({top: 10, right: 10, bottom: 20, left: 40})
    .height(300)
    .colors(darker_colors())
    .dimension(year)
    .group(year)
    .transitionDuration(300)
    .valueAccessor(function(d){return d.value.value})
    .x(d3.scale.linear().domain([2002,2015]))
    .centerBar(true)
    .xUnits(dc.units.integers)
    .ordering(function(d){ return d.key})
    .elasticY(true)

  year_chart.xAxis().ticks(12).tickFormat(function (d) {return d});
  year_chart.yAxis().tickFormat(d3.format('s')).ticks(12)

  // now the magic happens.
  datafox.registry.connect("{{ site.datafox_host }}",'moe', 'data');
  datafox.registry.registerAll();

  datafox.registry.fetchData(
    function() {
      dc.renderAll();
    }
  );
}

