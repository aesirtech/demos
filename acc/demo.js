---
# Front-matter so Jekyll processes it
---

var industry_chart, gender_chart, quarter_chart;

var dictionary = {};

var dateFormat = d3.time.format('%Y-%m');

queue()
    .defer(d3.csv, 'dict.csv')
    .await(showCharts)

function showCharts(error, dict) {
    for (entry of dict) {
        category = entry.category
        label = entry.label
        id = entry.start
        dictionary[category] = dictionary[category] || {}
        dictionary[category][+id] = label;
    }

    function labelAccessor(category, withValue) {
        return function(d) {
            var dict = dictionary[category];
            var label = dict[+d.key];
            if (withValue) {
                label = label + ": " + Math.floor(d.value.value);
            }
            return label;
        }
    }

    industry_data = datafox.fakeCrossFilter().dim('industry').aggregation({target:'value',type:'count','name':'value'}).order({name:'value'}).limit(12);
    industry_chart = dc.rowChart('#industry_chart')
        .height(300)
        .dimension(industry_data)
        .group(industry_data)
        .valueAccessor(function(d){return d.value.value})
        .title(labelAccessor('industry',true))
        .label(labelAccessor('industry'))
        .ordering(function(d){ return -d.value.value})
        .transitionDuration(200)
        .elasticX(true)

    industry_chart.xAxis().ticks(4).tickFormat(d3.format('s'))

    diagnosis_data = datafox.fakeCrossFilter().dim('diagnosis').aggregation({target:'value',type:'count','name':'value'}).order({name:'value'}).limit(12);
    diagnosis_chart = dc.rowChart('#diagnosis_chart')
        .height(300)
        .dimension(diagnosis_data)
        .group(diagnosis_data)
        .valueAccessor(function(d){return Math.floor(d.value.value)})
        .title(labelAccessor('diagnosis',true))
        .label(labelAccessor('diagnosis'))
        .ordering(function(d){ return -d.value.value})
        .transitionDuration(200)
        .elasticX(true)

    diagnosis_chart.xAxis().ticks(4).tickFormat(d3.format('s'))


    cause_data = datafox.fakeCrossFilter().dim('Cause').aggregation({target:'value',type:'count','name':'value'}).order({name:'value'}).limit(12);
    cause_chart = dc.rowChart('#cause_chart')
        .height(300)
        .dimension(cause_data)
        .group(cause_data)
        .valueAccessor(function(d){return d.value.value})
        .title(labelAccessor('cause',true))
        .label(labelAccessor('cause'))
        .ordering(function(d){ return -d.value.value})
        .transitionDuration(200)
        .elasticX(true)

    cause_chart.xAxis().ticks(4).tickFormat(d3.format('s'))


    bodysite_data = datafox.fakeCrossFilter().dim('bodysite').aggregation({target:'value',type:'count','name':'value'}).order({name:'value'}).limit(12);
    bodysite_chart = dc.rowChart('#bodysite_chart')
        .height(300)
        .dimension(bodysite_data)
        .group(bodysite_data)
        .valueAccessor(function(d){return d.value.value})
        .title(labelAccessor('bodysite',true))
        .label(labelAccessor('bodysite'))
        .ordering(function(d){ return -d.value.value})
        .transitionDuration(200)
        .elasticX(true)

    bodysite_chart.xAxis().ticks(4).tickFormat(d3.format('s'))

    fund_data = datafox.fakeCrossFilter().dim('Fund').aggregation({target:'value',type:'count','name':'value'}).order({name:'value'}).limit(12);
    fund_chart = dc.rowChart('#fund_chart')
        .height(300)
        .dimension(fund_data)
        .group(fund_data)
        .valueAccessor(function(d){return d.value.value})
        .title(labelAccessor('fund',true))
        .label(labelAccessor('fund'))
        .ordering(function(d){ return -d.value.value})
        .transitionDuration(200)
        .elasticX(true)

    fund_chart.xAxis().ticks(4).tickFormat(d3.format('s'))

    function ageOrder(key) {
        text = key.split('-')[0];
        text = text.replace("+","");
        if (text == "Unknown") return 100;
        return parseInt(text,10);
    }

    agegrp_data = datafox.fakeCrossFilter().dim('agegrp').aggregation({target:'value',type:'count','name':'value'}).order({name:'value'});
    agegrp_chart = dc.rowChart('#agegrp_chart')
        .height(300)
        .dimension(agegrp_data)
        .group(agegrp_data)
        .valueAccessor(function(d){return d.value.value})
        .title(labelAccessor('agegrp',true))
        .label(labelAccessor('agegrp'))
        .ordering(function(d){ return ageOrder(d.key)})
        .transitionDuration(200)
        .elasticX(true)

    agegrp_chart.xAxis().ticks(4).tickFormat(d3.format('s'))


    occupation_data = datafox.fakeCrossFilter().dim('Occupation').aggregation({target:'value',type:'count','name':'value'}).order({name:'value'}).limit(12);
    occupation_chart = dc.rowChart('#occupation_chart')
        .height(300)
        .dimension(occupation_data)
        .group(occupation_data)
        .valueAccessor(function(d){return d.value.value})
        .title(labelAccessor('occupation',true))
        .label(labelAccessor('occupation'))
        .ordering(function(d){ return -d.value.value})
        .transitionDuration(200)
        .elasticX(true)

    occupation_chart.xAxis().ticks(4).tickFormat(d3.format('s'))

    ethnicity_data = datafox.fakeCrossFilter().dim('ethnicity').aggregation({target:'value',type:'count','name':'value'}).order({name:'value'}).limit(12);
    ethnicity_chart = dc.rowChart('#ethnicity_chart')
        .height(300)
        .dimension(ethnicity_data)
        .group(ethnicity_data)
        .valueAccessor(function(d){return d.value.value})
        .title(labelAccessor('ethnicity',true))
        .label(labelAccessor('ethnicity'))
        .ordering(function(d){ return -d.value.value})
        .transitionDuration(200)
        .elasticX(true)

    ethnicity_chart.xAxis().ticks(4).tickFormat(d3.format('s'))

    gender_data = datafox.fakeCrossFilter().dim('gender').aggregation({target:'value',type:'count','name':'value'}).order({name:'value'}).limit(12);
    gender_chart = dc.pieChart('#gender_chart')
        .height(200)
        .radius(80)
        .innerRadius(50)
        .title(labelAccessor('gender', true))
        .label(labelAccessor('gender'))
        .dimension(gender_data)
        .group(gender_data)
        .transitionDuration(200)
        .valueAccessor(function(d){return d.value.value})

    quarter_data = datafox.fakeCrossFilter().dim('Quarter').aggregation({target:'value',type:'count','name':'value'}).order({name:'value'});
    quarter_chart = dc.barChart('#quarter_chart')
        .margins({top: 10, right: 10, bottom: 20, left: 40})
        .height(300)
        .dimension(quarter_data)
        .group(quarter_data)
        .valueAccessor(function(d){return d.value.value})
        .x(d3.scale.ordinal().domain(['2004-04','2014-01']))
        .xUnits(dc.units.ordinal)
        .elasticX(true)
        .ordering(function(d){ return d.key})
        .transitionDuration(200)
        .elasticY(true)

    quarter_chart.xAxis().ticks(12).tickFormat(function (d) {if (d[6] == '1') return d});
    quarter_chart.yAxis().tickFormat(d3.format('s')).ticks(12)


  // now the magic happens.
  datafox.registry.connect("{{ site.datafox_host }}",'acc','data');
  datafox.registry.registerAll();

  datafox.registry.fetchData(
    function() {
      dc.renderAll();
    }
  );
}
